package optionalTests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import edu.vanderbilt.imagecrawler.crawler.Assignment1aCrawlerTest;

/**
 * REQUIRED test suite for this assignment.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses(Assignment1aCrawlerTest.class)
public class Assignment1aOptionalTestSuite {
}
