package edu.vanderbilt.imagecrawler.utils;

import java.util.Spliterator;
import java.util.Spliterators;
import java.util.function.Consumer;

/**
 * Defines an object for traversing and partitioning
 * elements of an Array.
 */
public final class ArraySpliterator<E>
        extends Spliterators.AbstractSpliterator<E> {
    /**
     * The array to traverse and/or partition.
     */
    // TODO - you fill in here
    private final Array<E> mArray;

    /**
     * Current index, modified on advance/split.
     */
    // TODO - you fill in here
    private int mIndex;

    /**
     * One past the end of the spliterator range.
     */
    // TODO - you fill in here
    private final int mEnd;

    /**
     * Create new spliterator covering the given range.
     */
    ArraySpliterator(Array<E> array,
                     int origin,
                     int end) {
        super(array.size(),
                Spliterator.ORDERED | Spliterator.SIZED | Spliterator.SUBSIZED);

        // TODO - you fill in here.
        mArray = array;
        mIndex = origin;
        mEnd = end;
    }

    /**
     * If a remaining element exists, performs the given action on
     * it, returning true; else returns false.  Throw
     * NullPointerException of {@code action} is null.
     */
    public boolean tryAdvance(Consumer<? super E> action) {
        // TODO - you fill in here (replace 'return false' with proper code).
        if (action == null) {
            throw new NullPointerException();
        }

        if (mIndex < mEnd) {
            //noinspection unchecked
            action.accept((E) mArray.uncheckedToArray()[mIndex++]);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Returns a Spliterator covering elements that will, upon
     * return from this method, not be covered by this Spliterator.
     */
    public ArraySpliterator<E> trySplit() {
        // TODO - you fill in here (replace 'return null' with proper code).
        int hi = mEnd;
        int lo = mIndex;
        int mid = (lo + hi) >>> 1;

        if (lo >= mid) {
            // Divide range in half unless too small.
            return null;
        } else {
            return new ArraySpliterator<>(mArray, lo, mIndex = mid);
        }
    }
}
