#
# Common to all branches
#
git checkout master gradle
git checkout master .gitignore
git checkout master gradle.properties
git checkout master gradlew
git checkout master gradlew.bat
git checkout master settings.gradle
git checkout master settings.gradle

git checkout master app/build.gradle
git checkout master app/src/main
git checkout master app/src/androidTest/java/common

git checkout master image-crawler/build.gradle
git checkout master image-crawler/web-pages
git checkout master image-crawler/ground-truth
git checkout master image-crawler/src/main/java/edu/vanderbilt/imagecrawler/utils/AdminUtils.java
git checkout master image-crawler/src/main/java/edu/vanderbilt/imagecrawler/utils/Array.java
#git checkout master image-crawler/src/main/java/edu/vanderbilt/imagecrawler/utils/ArrayCollector.java
git checkout master image-crawler/src/main/java/edu/vanderbilt/imagecrawler/utils/Assignment.java
git checkout master image-crawler/src/main/java/edu/vanderbilt/imagecrawler/utils/BlockingTask.java
git checkout master image-crawler/src/main/java/edu/vanderbilt/imagecrawler/utils/CommandLineOptions.java
git checkout master image-crawler/src/main/java/edu/vanderbilt/imagecrawler/utils/ConcurrentHashSet.java
git checkout master image-crawler/src/main/java/edu/vanderbilt/imagecrawler/utils/Crawler.java
git checkout master image-crawler/src/main/java/edu/vanderbilt/imagecrawler/utils/DirectoryCrawler.java
git checkout master image-crawler/src/main/java/edu/vanderbilt/imagecrawler/utils/ExceptionUtils.java
#git checkout master image-crawler/src/main/java/edu/vanderbilt/imagecrawler/utils/FuturesCollector.java
#git checkout master image-crawler/src/main/java/edu/vanderbilt/imagecrawler/utils/FuturesCollectorStream.java
git checkout master image-crawler/src/main/java/edu/vanderbilt/imagecrawler/utils/Image.java
git checkout master image-crawler/src/main/java/edu/vanderbilt/imagecrawler/utils/IOUtils.java
git checkout master image-crawler/src/main/java/edu/vanderbilt/imagecrawler/utils/Options.java
git checkout master image-crawler/src/main/java/edu/vanderbilt/imagecrawler/utils/StreamsUtils.java
git checkout master image-crawler/src/main/java/edu/vanderbilt/imagecrawler/utils/SynchronizedArray.java
git checkout master image-crawler/src/main/java/edu/vanderbilt/imagecrawler/utils/UnsynchronizedArray.java
git checkout master image-crawler/src/main/java/edu/vanderbilt/imagecrawler/utils/UriUtils.java
git checkout master image-crawler/src/main/java/edu/vanderbilt/imagecrawler/utils/WebPageCrawler.java
git checkout master image-crawler/src/main/java/edu/vanderbilt/imagecrawler/utils/WebPageElement.java

git checkout master image-crawler/src/main/java/edu/vanderbilt/imagecrawler/crawlers/ImageCrawler.java
git checkout master image-crawler/src/main/java/edu/vanderbilt/imagecrawler/crawlers/CrawlerType.java
git checkout master image-crawler/src/main/java/edu/vanderbilt/imagecrawler/crawlers/SequentialLoopsCrawler.java

git checkout master image-crawler/src/main/java/edu/vanderbilt/imagecrawler/transforms
git checkout master image-crawler/src/main/java/edu/vanderbilt/imagecrawler/platform
git checkout master image-crawler/src/test/resources

#
# Assignment files.
#
git checkout master README-2a.html
git checkout master app/src/androidTest/java/assignment2aTests
git checkout master image-crawler/src/main/java/edu/vanderbilt/imagecrawler/crawlers/SequentialStreamsCrawler.java
git checkout master image-crawler/src/test/java/assignment2aTests
git checkout master image-crawler/src/test/java/edu/vanderbilt/imagecrawler/crawlers/SequentialStreamsCrawlerTests.kt
git checkout master image-crawler/src/test/java/edu/vanderbilt/imagecrawler/utils/UnsychronizedArrayTests.kt
git checkout master image-crawler/src/test/java/edu/vanderbilt/imagecrawler/utils/UnsynchronizedArrayIteratorTests.kt
git checkout master image-crawler/src/test/java/edu/vanderbilt/imagecrawler/utils/UnsynchronizedArrayJava8Tests.kt
git checkout master image-crawler/src/test/java/edu/vanderbilt/imagecrawler/utils/UnsynchronizedArraySpliteratorTests.kt
#git checkout master image-crawler/src/test/java/edu/vanderbilt/imagecrawler/utils/UnsynchronizedArrayCollectorTests.kt
git checkout master image-crawler/src/test/java/edu/vanderbilt/imagecrawler/utils/SynchronizedArrayTests.kt
git checkout master image-crawler/src/test/java/edu/vanderbilt/imagecrawler/utils/SynchronizedArrayTestsForGradsOnly.kt
